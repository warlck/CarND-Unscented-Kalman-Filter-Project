#include "ukf.h"
#include "Eigen/Dense"
#include <iostream>

using namespace std;

using Eigen::MatrixXd;
using Eigen::VectorXd;
using std::vector;

/**
 * Initializes Unscented Kalman filter
 */
UKF::UKF() {
  n_x_ = 5;
  n_aug_ = 7;
  lambda_ = 3 - n_aug_;
  is_initialized_ = false;

  // if this is false, laser measurements will be ignored (except during init)
  use_laser_ = true;

  // if this is false, radar measurements will be ignored (except during init)
  use_radar_ = true;

  // initial state vector
  x_ = VectorXd(n_x_);

  // initial covariance matrix
  P_ = MatrixXd(n_x_, n_x_);

  // initial predicted sigma points
  Xsig_pred_ = MatrixXd(n_x_, 2 * n_aug_ + 1);

  // Process noise standard deviation longitudinal acceleration in m/s^2
  std_a_ = 3;

  // Process noise standard deviation yaw acceleration in rad/s^2
  std_yawdd_ = 2;

  // Laser measurement noise standard deviation position1 in m
  std_laspx_ = 0.15;

  // Laser measurement noise standard deviation position2 in m
  std_laspy_ = 0.15;

  // Radar measurement noise standard deviation radius in m
  std_radr_ = 0.3;

  // Radar measurement noise standard deviation angle in rad
  std_radphi_ = 0.03;

  // Radar measurement noise standard deviation radius change in m/s
  std_radrd_ = 0.3;


  // Initial covariance matrix 
  P_ = MatrixXd::Identity(5,5);

  weights_ = VectorXd(2 * n_aug_+1);

  double weight_0 = lambda_/(lambda_+n_aug_);
  weights_(0) = weight_0;
  for (int i=1; i<2*n_aug_+1; i++) {  //2n+1 weights
    double weight = 0.5/(n_aug_+lambda_);
    weights_(i) = weight;
  }


  Radar_covariance_ = MatrixXd(3,3);
  Radar_covariance_ <<    
          std_radr_*std_radr_, 0, 0,
          0, std_radphi_*std_radphi_, 0,
          0, 0,std_radrd_*std_radrd_;

  Lidar_covariance_ = MatrixXd(2,2);
  Lidar_covariance_ <<    
          std_laspx_*std_laspx_, 0,
          0, std_laspy_*std_laspy_;
}

UKF::~UKF() {}

/**
 * @param {MeasurementPackage} meas_package The latest measurement data of
 * either radar or laser.
 */
void UKF::ProcessMeasurement(MeasurementPackage meas_package) {
  /**
  TODO:

  Complete this function! Make sure you switch between lidar and radar
  measurements.
  */

  if (!is_initialized_) {
    // first measurement

    x_.fill(0.0);

    if (meas_package.sensor_type_ == MeasurementPackage::RADAR ) {

      if (!use_radar_) 
        return;

      /**
      Convert radar from polar to cartesian coordinates and initialize state.
      */
      float ro     = meas_package.raw_measurements_(0);
      float phi    = meas_package.raw_measurements_(1);
      float ro_dot = meas_package.raw_measurements_(2);
      x_(0) = ro * cos(phi);
      x_(1) = ro * sin(phi);      
      x_(2) = 0;
      x_(3) = 0;
      x_(4) = 0;
    }
    else if (meas_package.sensor_type_ == MeasurementPackage::LASER ) {
      if (!use_laser_)
        return;
      /**
      Initialize state.
      */
      
      x_(0) = meas_package.raw_measurements_(0);
      x_(1) = meas_package.raw_measurements_(1);
      
    }

    // done initializing, no need to predict or update
    previous_timestamp_ = meas_package.timestamp_;
    is_initialized_ = true;
    return;
  }

  float dt = (meas_package.timestamp_ - previous_timestamp_) / 1000000.0; //dt - expressed in seconds
  previous_timestamp_ = meas_package.timestamp_;



  Prediction(dt);
  

  if (meas_package.sensor_type_ == MeasurementPackage::RADAR ) {
    if (!use_radar_) 
      return;

    // Radar updates
    UpdateRadar(meas_package);

  } else {
    if (!use_laser_)
      return;

    // Laser updates
    UpdateLidar(meas_package);
  }

}

/**
 * Predicts sigma points, the state, and the state covariance matrix.
 * @param {double} delta_t the change in time (in seconds) between the last
 * measurement and this one.
 */


void UKF::GenerateSigmaPoints(double delta_t, MatrixXd* Xsig_out) {
  VectorXd x_aug = VectorXd(n_aug_);
  MatrixXd P_aug = MatrixXd(n_aug_, n_aug_);
  MatrixXd Xsig_aug = MatrixXd(n_aug_, 2 * n_aug_ + 1);

  //create augmented mean state
  x_aug.head(n_x_) = x_; 
  x_aug(n_x_ ) = 0;
  x_aug(n_x_ + 1) = 0;

  //create augmented covariance matrix
  P_aug.fill(0.0);
  P_aug.topLeftCorner(5,5) = P_;
  P_aug(5,5) = std_a_*std_a_;
  P_aug(6,6) = std_yawdd_*std_yawdd_;

  //create square root matrix
  MatrixXd A = P_aug.llt().matrixL();
  //create augmented sigma points
  Xsig_aug.col(0) = x_aug;
  
  for (int i = 0; i < n_aug_; i++) {
      Xsig_aug.col(i+1)       = x_aug + sqrt(lambda_+n_aug_)*A.col(i);
      Xsig_aug.col(i+1+n_aug_) = x_aug - sqrt(lambda_+n_aug_)*A.col(i);
  }

  *Xsig_out  = Xsig_aug;
}

void UKF::SigmaPointsPrediction(double delta_t, const MatrixXd& Xsig_aug) {
  for (int i = 0; i< 2*n_aug_+1; i++)
  {
    //extract values for better readability
    const double p_x = Xsig_aug(0,i);
    const double p_y = Xsig_aug(1,i);
    const double v = Xsig_aug(2,i);
    const double yaw = Xsig_aug(3,i);
    const double yawd = Xsig_aug(4,i);
    const double nu_a = Xsig_aug(5,i);
    const double nu_yawdd = Xsig_aug(6,i);

    //predicted state values
    double px_p, py_p;

    //avoid division by zero
    if (fabs(yawd) > 0.001) {
        px_p = p_x + v/yawd * ( sin (yaw + yawd*delta_t) - sin(yaw));
        py_p = p_y + v/yawd * ( cos(yaw) - cos(yaw+yawd*delta_t) );
    }
    else {
        px_p = p_x + v*delta_t*cos(yaw);
        py_p = p_y + v*delta_t*sin(yaw);
    }

    double v_p = v;
    double yaw_p = yaw + yawd*delta_t;
    double yawd_p = yawd;

    //add noise
    px_p = px_p + 0.5*nu_a*delta_t*delta_t * cos(yaw);
    py_p = py_p + 0.5*nu_a*delta_t*delta_t * sin(yaw);
    v_p = v_p + nu_a*delta_t;

    yaw_p = yaw_p + 0.5*nu_yawdd*delta_t*delta_t;
    yawd_p = yawd_p + nu_yawdd*delta_t;

    //write predicted sigma point into right column
    Xsig_pred_(0,i) = px_p;
    Xsig_pred_(1,i) = py_p;
    Xsig_pred_(2,i) = v_p;
    Xsig_pred_(3,i) = yaw_p;
    Xsig_pred_(4,i) = yawd_p;
  }
}

void NormalizeAngle(double& phi) {
  phi = atan2(sin(phi), cos(phi));
}

void UKF::PredictMeanAndCovariance() {
  //predicted state mean
  x_.fill(0.0);
  for (int i = 0; i < 2 * n_aug_ + 1; i++) {  //iterate over sigma points
    x_ = x_+ weights_(i) * Xsig_pred_.col(i);
  }

  //predicted state covariance matrix
  P_.fill(0.0);
  for (int i = 0; i < 2 * n_aug_ + 1; i++) {  //iterate over sigma points

    // state difference
    VectorXd x_diff = Xsig_pred_.col(i) - x_;
    //angle normalization
    NormalizeAngle(x_diff(3));

    P_ = P_ + weights_(i) * x_diff * x_diff.transpose();
  }
}


void UKF::Prediction(double delta_t) {
  MatrixXd Xsig_aug;
  GenerateSigmaPoints(delta_t, &Xsig_aug);
  SigmaPointsPrediction(delta_t, Xsig_aug);
  PredictMeanAndCovariance();
}




void UKF::PredictLidarMeasurement(MatrixXd& S, VectorXd& z_pred, MatrixXd& Zsig, MatrixXd& R) {
 
  //transform sigma points into measurement space
  for (int i = 0; i < 2 * n_aug_ + 1; i++) {  //2n+1 simga points

    // extract values for better readibility
    double p_x = Xsig_pred_(0,i);
    double p_y = Xsig_pred_(1,i);

    // measurement model
    Zsig(0,i) = p_x;              
    Zsig(1,i) = p_y;              
  }


  //mean predicted measurement
  z_pred.fill(0.0);

  z_pred(0) = x_(0);
  z_pred(1) = x_(1);

  //measurement covariance matrix S
  S.fill(0.0);
  for (int i = 0; i < 2 * n_aug_ + 1; i++) {  //2n+1 simga points
    //residual
    VectorXd z_diff = Zsig.col(i) - z_pred;
    S = S + weights_(i) * z_diff * z_diff.transpose();
  }

  //add measurement noise covariance matrix
  S = S + R;
}


void UKF::UpdateStateLidar(MeasurementPackage meas_package, MatrixXd& Tc, MatrixXd& Zsig, MatrixXd& S, VectorXd& z_pred) {
  //calculate cross correlation matrix
  Tc.fill(0.0);

  for (int i = 0; i < 2 * n_aug_ + 1; i++) {  //2n+1 sigma points
    //residual
    VectorXd z_diff = Zsig.col(i) - z_pred;
    // state difference
    VectorXd x_diff = Xsig_pred_.col(i) - x_;
    //angle normalization
    NormalizeAngle(x_diff(3));

    Tc = Tc + weights_(i) * x_diff * z_diff.transpose();
  }

  //Kalman gain K;
  MatrixXd K = Tc * S.inverse();


  VectorXd z  = VectorXd(2);
  
  z <<  
    meas_package.raw_measurements_(0),
    meas_package.raw_measurements_(1);
  
 
  //residual
  VectorXd z_diff = z - z_pred;

  //update state mean and covariance matrix
  x_ = x_ + K * z_diff;
  P_ = P_ - K*S*K.transpose();

}

/**
 * Updates the state and the state covariance matrix using a laser measurement.
 * @param {MeasurementPackage} meas_package
 */
void UKF::UpdateLidar(MeasurementPackage meas_package) {
  /**
  TODO:

  Complete this function! Use lidar data to update the belief about the object's
  position. Modify the state vector, x_, and covariance, P_.

  You'll also need to calculate the lidar NIS.
  */

    // radar measurement vector dimesion
  int n_z_ = 2; 

  MatrixXd S = MatrixXd(n_z_,n_z_);
  VectorXd z_pred = VectorXd(n_z_);
  MatrixXd Zsig = MatrixXd(n_z_, 2 * n_aug_ + 1);

  PredictLidarMeasurement(S, z_pred,Zsig, Lidar_covariance_);

  MatrixXd Tc = MatrixXd(n_x_, n_z_);
  UpdateStateLidar(meas_package, Tc, Zsig, S, z_pred);

}

/**
 * Updates the state and the state covariance matrix using a radar measurement.
 * @param {MeasurementPackage} meas_package
 */

void UKF::PredictRadarMeasurement(MatrixXd& S, VectorXd& z_pred, MatrixXd& Zsig, MatrixXd& R) {
  //transform sigma points into measurement space
  for (int i = 0; i < 2 * n_aug_ + 1; i++) {  //2n+1 simga points

    // extract values for better readibility
    double p_x = Xsig_pred_(0,i);
    double p_y = Xsig_pred_(1,i);
    double v  = Xsig_pred_(2,i);
    double yaw = Xsig_pred_(3,i);

    double v1 = cos(yaw)*v;
    double v2 = sin(yaw)*v;
    double rho = sqrt(p_x*p_x + p_y*p_y); 

    double eps = 0.00001;

    // measurement model
    Zsig(0,i) = rho;                          //r

    if (p_x == 0) {                            //phi
      Zsig(1,i) = atan2(p_y,eps);  
    } else {
      Zsig(1,i) = atan2(p_y, p_x);
    }
                
    Zsig(2,i) = (p_x*v1 + p_y*v2) / std::max(eps, rho) ;   //r_dot
  }

  //mean predicted measurement
 
  z_pred.fill(0.0);
  for (int i=0; i < 2*n_aug_+1; i++) {
      z_pred = z_pred + weights_(i) * Zsig.col(i);
  }

  //measurement covariance matrix S
  S.fill(0.0);
  for (int i = 0; i < 2 * n_aug_ + 1; i++) {  //2n+1 simga points
    //residual
    VectorXd z_diff = Zsig.col(i) - z_pred;

    //angle normalization
     NormalizeAngle(z_diff(1));

    S = S + weights_(i) * z_diff * z_diff.transpose();
  }

  //add measurement noise covariance matrix
  S = S + R;
}


void UKF::UpdateStateRadar(MeasurementPackage meas_package, MatrixXd& Tc, MatrixXd& Zsig, MatrixXd& S, VectorXd& z_pred) {
  //calculate cross correlation matrix
  Tc.fill(0.0);
  for (int i = 0; i < 2 * n_aug_ + 1; i++) {  //2n+1 simga points

    //residual
    VectorXd z_diff = Zsig.col(i) - z_pred;
    //angle normalization
     NormalizeAngle(z_diff(1));

    // state difference
    VectorXd x_diff = Xsig_pred_.col(i) - x_;
    //angle normalization
     NormalizeAngle(x_diff(3));

    Tc = Tc + weights_(i) * x_diff * z_diff.transpose();
  }

  //Kalman gain K;
  MatrixXd K = Tc * S.inverse();


  VectorXd z  = VectorXd(3);
  
  z <<  
    meas_package.raw_measurements_(0),
    meas_package.raw_measurements_(1),
    meas_package.raw_measurements_(2);
  
 
  //residual
  VectorXd z_diff = z - z_pred;

  //angle normalization
   NormalizeAngle(z_diff(1));

  //update state mean and covariance matrix
  x_ = x_ + K * z_diff;
  P_ = P_ - K*S*K.transpose();
}


void UKF::UpdateRadar(MeasurementPackage meas_package) {
  // radar measurement vector dimesion
  int n_z_ = 3; 

  MatrixXd S = MatrixXd(n_z_,n_z_);
  VectorXd z_pred = VectorXd(n_z_);
  MatrixXd Zsig = MatrixXd(n_z_, 2 * n_aug_ + 1);



  PredictRadarMeasurement(S, z_pred,Zsig, Radar_covariance_);

  MatrixXd Tc = MatrixXd(n_x_, n_z_);
  UpdateStateRadar(meas_package, Tc, Zsig, S, z_pred);
}
